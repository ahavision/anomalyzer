package ahavision

import (
	"bitbucket.org/ahavision/anomalyzer/cfg"
	"fmt"
	//@TODO remove this after resolve https://github.com/RecastAI/SDK-Golang/issues/15
	"github.com/Slach/Recast-Golang-SDK/recast"
	//"github.com/RecastAI/SDK-Golang/recast"

	"github.com/rs/zerolog/log"
	"time"
)

//RecastReporter send message to recast.ai chatbot config.Reporter.recastConversationIds
type RecastReporter struct {
	ReporterInterface
}

//ReportAnomaly send message to recast.ai chatbot config.Reporter.recastConversationIds
func (r *RecastReporter) ReportAnomaly(
	config *cfg.ConfigType,
	anomaly *AnomalyType,
	appID, anomalyName, categoryDimension, categoryValue, metricName, metricValue string,
	dateNames []time.Time, tsData []float64, nestedTS map[string]map[string][]float64,
	probability, impactPercent float64,
) {
	defer dataSyncronizer.reportData.Done()
	client := recast.ConnectClient{Token: config.Reporter.RecastRequestToken}

	reportName := cleanFileName(fmt.Sprintf(
		"main.%s.%s.%s.from.%s.to.%s",
		metricName,
		categoryDimension,
		categoryValue,
		dateNames[0].Format("2006-01-02"),
		dateNames[len(dateNames)-1].Format("2006-01-02"),
	))

	carousel := recast.NewCarousel()

	card := recast.NewCarouselCard(
		fmt.Sprintf("%v=%v", categoryDimension, categoryValue),
		fmt.Sprintf("%v anomaly=%0.2f%% impact=%0.2f%%", metricName, probability*100, impactPercent*100),
	)

	card.AddImage(fmt.Sprintf("%s/%s/%s/%s.png", config.Reporter.BaseURL, appID, anomalyName, reportName)).
		AddButton("Mute", "postback", fmt.Sprintf("/mute %s", reportName))

	carousel.AddCard(card)

	for nestedDimension, nestedValues := range nestedTS {
		if len(nestedValues) == 0 {
			continue
		}
		nestedName := cleanFileName(fmt.Sprintf(
			"nested.%s.%s.from.%s.to.%s",
			metricName,
			nestedDimension,
			dateNames[0].Format("2006-01-02"),
			dateNames[len(dateNames)-1].Format("2006-01-02"),
		))
		card = recast.NewCarouselCard(
			fmt.Sprintf("%v=%v", categoryDimension, categoryValue),
			fmt.Sprintf("%v anomaly=%0.2f%% impact=%0.2f%%", metricName, probability*100, impactPercent*100),
		).AddImage(fmt.Sprintf("%s/%s/%s/%s.png", config.Reporter.BaseURL, appID, anomalyName, nestedName))
		carousel.AddCard(card)
	}

	for _, conversationID := range config.Reporter.RecastConversationIDs {
		if err := client.SendMessage(conversationID, carousel); err != nil {
			log.Error().Err(err).Msg("recast bot SendMessage error")
		}
		time.Sleep(time.Second)
	}
}
