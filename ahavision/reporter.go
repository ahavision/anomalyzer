package ahavision

import (
	"bitbucket.org/ahavision/anomalyzer/cfg"
	"fmt"
	"time"
)

//ReporterInterface common interface for anomaly reporting
type ReporterInterface interface {
	ReportAnomaly(
		config *cfg.ConfigType,
		anomaly *AnomalyType,
		appID, anomalyName, categoryDimension, categoryValue, metricName, metricValue string,
		dateNames []time.Time, tsData []float64, nestedTS map[string]map[string][]float64,
		probability, impactPercent float64,
	)
}

func getReporterByKey(key string) (reporter ReporterInterface, err error) {
	switch key {
	case "file", "files":
		r := FileReporter{}
		return &r, nil
	case "recast":
		r := RecastReporter{}
		return &r, nil
	}
	return nil, fmt.Errorf("invalid reportType key=%s", key)
}
