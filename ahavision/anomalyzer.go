package ahavision

import (
	"database/sql"
	"fmt"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"bitbucket.org/ahavision/anomalyzer/cfg"
	"bitbucket.org/clickhouse_pro/components/chpool"

	"github.com/lytics/anomalyzer"
	"github.com/rs/zerolog/log"
	"gopkg.in/go-playground/pool.v3"
)

var dataSyncronizer = struct {
	detectGoRoutinesPool pool.Pool
	reportData           *sync.WaitGroup
}{}

// InitDataSyncronyzer init go-playground/pool.v3
func InitDataSyncronyzer(config *cfg.ConfigType) {
	dataSyncronizer.detectGoRoutinesPool = pool.NewLimited(config.Anomalyzer.ParallelJob)
	dataSyncronizer.reportData = &sync.WaitGroup{}
}

var anomaliesFound uint64

// DetectAnomalies @TODO 4h/DEV think about multiple dimension for correlation!
func DetectAnomalies(config *cfg.ConfigType) {
	anomaliesFound = 0
	defer dataSyncronizer.detectGoRoutinesPool.Close()
	defer dataSyncronizer.reportData.Wait()
	defer func() {
		log.Info().Uint64("anomaliesFound", anomaliesFound).Msg("DetectAnomalies DONE")
	}()

	jobBatch := dataSyncronizer.detectGoRoutinesPool.Batch()

	go func() {
		tsLenght := int(config.DateUntil.Sub(config.DateSince).Hours()/24) + 1
		dateIndex, dateNames := prepareDateIndex(tsLenght, config)

		for anomalyName, anomaly := range AnomalyDefinitionsConfig.AnomaliesDefinitions {
			for _, appID := range getAppIds(config, anomaly) {
				prepareJobBatch(anomaly, config, anomalyName, appID, dateIndex, dateNames, tsLenght, jobBatch)
			}
		}
		jobBatch.QueueComplete()
	}()

	for res := range jobBatch.Results() {
		if err := res.Error(); err != nil {
			log.Error().Err(err).Msg("jobBacth.Results() error")
		}
	}

}
func prepareDateIndex(tsLenght int, config *cfg.ConfigType) (map[time.Time]int, []time.Time) {
	dateIndex := make(map[time.Time]int)
	dateNames := make([]time.Time, tsLenght)
	i := 0
	for d := config.DateSince; d.Before(config.DateUntil); d = d.AddDate(0, 0, 1) {
		dateIndex[d] = i
		dateNames[i] = d
		i++
	}
	return dateIndex, dateNames
}
func prepareJobBatch(anomaly *AnomalyType, config *cfg.ConfigType, anomalyName string, appID string, dateIndex map[time.Time]int, dateNames []time.Time, tsLenght int, jobBatch pool.Batch) {
	for _, dateDimension := range anomaly.DateDimensions {
		tsGlobalMetricsValues := prepareGlobalMetricsValues(config, anomaly, anomalyName, dateDimension, dateIndex, appID, tsLenght)
		for _, categoryGroup := range anomaly.CategoryDimensions {
			for categoryDimension, nestedDimensions := range categoryGroup {
				tsSQL := prepareTimeSeriesSQL(
					config, anomaly.Templates.TimeSeries, anomaly, categoryDimension, dateDimension, appID,
				)
				distinctCategorySQL := prepareDistinctCategorySQL(
					config, anomaly, categoryDimension, appID,
				)

				nestedSlice := strings.Split(nestedDimensions, ",")
				nestedSQL := make(map[string]string, len(nestedSlice))
				nestedDistinctSQL := make(map[string]string, len(nestedSlice))
				for _, nestedDimension := range nestedSlice {
					if nestedDimension != "" {
						nestedSQL[nestedDimension] = prepareNestedSQL(
							config, anomaly, nestedDimension, categoryDimension, dateDimension, appID,
						)
						nestedDistinctSQL[nestedDimension] = prepareDistinctNestedCategorySQL(
							config, anomaly, nestedDimension, categoryDimension, appID,
						)
					}
				}

				job := detectAnomaly(
					config,
					anomaly, anomalyName, appID, categoryDimension, dateDimension, tsSQL, distinctCategorySQL,
					nestedSQL, nestedDistinctSQL,
					dateIndex, dateNames, tsGlobalMetricsValues, tsLenght,
				)
				jobBatch.Queue(job)
			}
		}
	}
}

func prepareGlobalMetricsValues(
	config *cfg.ConfigType, anomaly *AnomalyType, anomalyName string,
	dateDimension *DateDimenstionType, dateIndex map[time.Time]int,
	appID string, tsLenght int,
) (tsGlobalMetricsValues map[string][]float64) {
	var err error
	defer func() {
		if err != nil {
			log.Fatal().Err(err).Str("anomalyName", anomalyName).Msg("prepareGlobalMetricsValues failed")
		}
	}()
	tsGlobalMetricsSQL := chpool.FormatSQLTemplate(
		anomaly.Templates.GlobalMetricsValues,
		map[string]interface{}{
			"db":                      config.Clickhouse.Database,
			"table":                   getTableNameSQL(config, anomaly.Table),
			"metrics":                 getMetricsSQL(anomaly),
			"date_dimensions":         getDateDimensionsSQL(dateDimension),
			"where":                   getTimeseriesWhereSQL(config, anomaly, appID),
			"date_dimensions_groupby": getDateDimensionsGroupBySQL(dateDimension),
		},
	)

	db := chpool.GetRandomClickhouseConnection()
	// metricName -> [dIdx]tsValue
	tsGlobalMetricsValues = make(map[string][]float64, len(anomaly.Metrics))
	for metricName := range anomaly.Metrics {
		tsGlobalMetricsValues[metricName] = make([]float64, tsLenght)
	}

	rows, err := db.Query(tsGlobalMetricsSQL)
	if err != nil {
		return nil
	}
	cols, _ := rows.Columns()
	for rows.Next() {
		r, err := chpool.FetchRowAsMap(rows, cols)
		if err != nil {
			return nil
		}
		fetchGlobalTsData(r, dateDimension, dateIndex, tsLenght, anomaly.Metrics, tsGlobalMetricsValues)
	}

	return tsGlobalMetricsValues
}

func detectAnomaly(
	config *cfg.ConfigType,
	anomaly *AnomalyType, anomalyName, appID, categoryDimension string, dateDimension *DateDimenstionType,
	tsSQL, distinctCategorySQL string,
	nestedSQL map[string]string, nestedDistinctSQL map[string]string,
	dateIndex map[time.Time]int, dateNames []time.Time,
	tsGlobalMetricsValues map[string][]float64, tsLenght int,
) pool.WorkFunc {

	return func(wu pool.WorkUnit) (interface{}, error) {
		defer log.Debug().
			Str("appID", appID).
			Str("categoryDimension", categoryDimension).
			Str("anomalyName", anomalyName).
			Msg("detectAnomaly end")

		log.Debug().
			Str("appID", appID).
			Str("categoryDimension", categoryDimension).
			Str("anomalyName", anomalyName).
			Msg("detectAnomaly begin")

		if wu.IsCancelled() {
			return nil, nil
		}
		db := chpool.GetRandomClickhouseConnection()
		// @TODO 4h/DEV need reduce calls for SELECT DISTINCT
		distinctValues, err := fetchDistinctCategoryValues(db, distinctCategorySQL)
		if err != nil {
			return false, err
		}
		// format  category_value -> metricName -> [dIdx]tsValue
		tsData := make(map[string]map[string][]float64)
		if err = prepareTsData(db, distinctValues, tsSQL, categoryDimension, dateDimension, dateIndex, tsLenght, anomaly.Metrics, tsData); err != nil {
			return false, err
		}

		for _, cValue := range distinctValues {
			for metricName, metricValue := range anomaly.Metrics {

				err = detectSingleTimeSeriesAnomaly(
					config, db, anomaly,
					appID, anomalyName,
					dateDimension, dateIndex,
					categoryDimension, cValue, metricName, metricValue,
					dateNames,
					nestedSQL, nestedDistinctSQL,
					tsData, tsGlobalMetricsValues, tsLenght,
				)
				if err != nil {
					return false, err
				}
			}
		}
		if err != nil {
			return false, err
		}
		return true, nil
	}
}

func detectSingleTimeSeriesAnomaly(
	config *cfg.ConfigType, db *sql.DB,
	anomaly *AnomalyType,
	appID, anomalyName string,
	dateDimension *DateDimenstionType, dateIndex map[time.Time]int,
	categoryDimension, cValue, metricName, metricValue string,
	dateNames []time.Time,
	nestedSQL, nestedDistinctSQL map[string]string,
	tsData map[string]map[string][]float64, tsGlobalMetricsValues map[string][]float64, tsLenght int,
) (err error) {
	var detector anomalyzer.Anomalyzer
	detector, err = anomalyzer.NewAnomalyzer(anomaly.AnomalyzerConf, tsData[cValue][metricName])
	if err != nil {
		return err
	}
	probability := detector.Eval()

	if probability >= config.Anomalyzer.Probability {
		impactPercent := caclulateDiffTimeSeries(tsGlobalMetricsValues[metricName], tsData[cValue][metricName])
		if impactPercent >= config.Anomalyzer.ImpactPercent {
			log.Debug().
				Str("categoryDimension", categoryDimension).
				Str("cValue", cValue).
				Str("anomalyName", anomalyName).
				Str("metricName", metricName).
				Str("metricValue", metricValue).
				Msgf("detectSingleTimeSeriesAnomaly probability=%v impactPercent=%v", probability, impactPercent)
			atomic.AddUint64(&anomaliesFound, 1)
			// format nestedDimension -> nValue -> [dIdx]metricValue
			nestedTS := make(map[string]map[string][]float64, len(nestedSQL))
			err = detectNestedAnomalies(
				config, db,
				anomaly,
				dateDimension, dateIndex,
				categoryDimension, cValue, metricValue, metricName,
				nestedSQL, nestedDistinctSQL, nestedTS, tsLenght,
			)
			if err != nil {
				return err
			}
			for _, reporterType := range config.Reporter.Types {
				var reporter ReporterInterface
				if reporter, err = getReporterByKey(reporterType); err == nil {
					dataSyncronizer.reportData.Add(1)
					go reporter.ReportAnomaly(
						config,
						anomaly,
						appID, anomalyName,
						categoryDimension, cValue,
						metricName, metricValue,
						dateNames, tsData[cValue][metricName],
						nestedTS,
						probability, impactPercent,
					)
				} else {
					return err
				}
			}
		}
	}
	return nil
}

func caclulateDiffTimeSeries(mainValues []float64, tsValues []float64) (impactPercent float64) {
	for i, v := range tsValues {
		p := v / mainValues[i]
		if p > impactPercent {
			impactPercent = p
		}
	}
	return impactPercent
}

func detectNestedAnomalies(
	config *cfg.ConfigType, db *sql.DB,
	anomaly *AnomalyType,
	dateDimension *DateDimenstionType, dateIndex map[time.Time]int,
	categoryDimension, cValue, metricValue, metricName string,
	nestedSQL map[string]string, nestedDistinctSQL map[string]string,
	nestedTS map[string]map[string][]float64,
	tsLenght int,
) (err error) {
	for nestedDimension, nestedDistinctTemplate := range nestedDistinctSQL {
		var nestedDistinctValues []string
		nestedDistinctValues, err = fetchNestedDistinctCategoryValues(db, nestedDistinctTemplate, cValue)
		if err != nil {
			return err
		}
		nestedTS[nestedDimension] = make(map[string][]float64, len(nestedDistinctValues))
		for _, nValue := range nestedDistinctValues {
			nestedTS[nestedDimension][nValue] = make([]float64, tsLenght)
		}
		q := chpool.FormatSQLTemplate(nestedSQL[nestedDimension], map[string]interface{}{
			"metrics": fmt.Sprintf("%s AS %s", metricValue, metricName),
		})
		rows, err := db.Query(q, cValue)
		if err != nil {
			return err
		}
		cols, _ := rows.Columns()
		for rows.Next() {
			r, err := chpool.FetchRowAsMap(rows, cols)
			if err != nil {
				return err
			}
			fetchNestedTsData(r, nestedDimension, dateDimension, dateIndex, tsLenght, metricName, nestedTS)
		}
		for _, nValue := range nestedDistinctValues {
			detector, err := anomalyzer.NewAnomalyzer(anomaly.AnomalyzerConf, nestedTS[nestedDimension][nValue])
			if err != nil {
				return err
			}
			nestedProbability := detector.Eval()
			if nestedProbability < config.Anomalyzer.NestedProbability {
				log.Debug().
					Float64("nestedProbability", nestedProbability).
					Str(categoryDimension, cValue).
					Str(nestedDimension, nValue).
					Floats64("nestedTS", nestedTS[nestedDimension][nValue]).
					Msgf("delete(nestedTS[%s], %s)", nestedDimension, nValue)
				delete(nestedTS[nestedDimension], nValue)
			}
		}
	}
	return nil
}
func prepareTsData(
	db *sql.DB, distinctValues []string, tsSQL string,
	categoryDimension string, dateDimension *DateDimenstionType, dateIndex map[time.Time]int, tsLenght int,
	metrics map[string]string, tsData map[string]map[string][]float64,
) (err error) {
	for _, cValue := range distinctValues {
		tsData[cValue] = make(map[string][]float64)
		for metricName := range metrics {
			tsData[cValue][metricName] = make([]float64, tsLenght)
		}
	}
	rows, err := db.Query(tsSQL)
	if err != nil {
		return err
	}
	cols, _ := rows.Columns()
	for rows.Next() {
		r, err := chpool.FetchRowAsMap(rows, cols)
		if err != nil {
			return err
		}
		fetchTsData(r, categoryDimension, dateDimension, dateIndex, tsLenght, metrics, tsData)
	}
	return nil
}

func fetchTsData(
	r map[string]interface{},
	categoryDimension string, dateDimension *DateDimenstionType, dateIndex map[time.Time]int, tsLenght int,
	metrics map[string]string, tsData map[string]map[string][]float64,
) {
	cValue := r[categoryDimension].(string)
	dValue := r[dateDimension.Name].(time.Time).UTC()
	dIdx, dExists := dateIndex[dValue]
	if !dExists || dIdx > tsLenght {
		log.Debug().Int("dIdx", dIdx).Time("dValue", dValue).Int("tsLength", tsLenght).Msgf("MISSED dateIndex=%v", dateIndex)
	}
	for metricName := range metrics {
		tsData[cValue][metricName][dIdx] = r[metricName].(float64)
	}
}

func fetchGlobalTsData(
	r map[string]interface{},
	dateDimension *DateDimenstionType, dateIndex map[time.Time]int, tsLenght int,
	metrics map[string]string, tsGlobalMetrics map[string][]float64,
) {
	dValue := r[dateDimension.Name].(time.Time).UTC()
	dIdx, dExists := dateIndex[dValue]
	if !dExists || dIdx > tsLenght {
		log.Debug().Int("dIdx", dIdx).Time("dValue", dValue).Int("tsLength", tsLenght).Msgf("MISSED dateIndex=%v", dateIndex)
	}
	for metricName := range metrics {
		tsGlobalMetrics[metricName][dIdx] = r[metricName].(float64)
	}
}

func fetchNestedTsData(
	r map[string]interface{},
	nestedDimension string, dateDimension *DateDimenstionType, dateIndex map[time.Time]int, tsLenght int,
	metricName string, nestedTS map[string]map[string][]float64,
) {
	nValue := r[nestedDimension].(string)
	dValue := r[dateDimension.Name].(time.Time).UTC()
	dIdx, dExists := dateIndex[dValue]
	if !dExists || dIdx > tsLenght {
		log.Debug().Int("dIdx", dIdx).Time("dValue", dValue).Int("tsLength", tsLenght).Msgf("MISSED dateIndex=%v", dateIndex)
	}
	nestedTS[nestedDimension][nValue][dIdx] = r[metricName].(float64)
}

func fetchDistinctCategoryValues(db *sql.DB, distinctCategorySQL string) ([]string, error) {
	distinctRows, err := db.Query(distinctCategorySQL)
	if err != nil {
		return nil, err
	}
	distinctValues := make([]string, 0)
	for distinctRows.Next() {
		var v string
		if err = distinctRows.Scan(&v); err != nil {
			return distinctValues, err
		}
		distinctValues = append(distinctValues, v)
	}
	return distinctValues, nil
}

func fetchNestedDistinctCategoryValues(db *sql.DB, distinctCategorySQL, categoryValue string) ([]string, error) {
	distinctRows, err := db.Query(distinctCategorySQL, categoryValue)
	if err != nil {
		return nil, err
	}
	distinctValues := make([]string, 0)
	for distinctRows.Next() {
		var v string
		if err = distinctRows.Scan(&v); err != nil {
			return distinctValues, err
		}
		distinctValues = append(distinctValues, v)
	}
	return distinctValues, nil
}

func prepareDistinctNestedCategorySQL(
	config *cfg.ConfigType, anomaly *AnomalyType, nestedDimension, categoryDimension string, appID string,
) string {
	return chpool.FormatSQLTemplate(
		anomaly.Templates.NestedDistinctValues,
		map[string]interface{}{
			"db":                 config.Clickhouse.Database,
			"table":              getTableNameSQL(config, anomaly.Table),
			"category_dimension": categoryDimension,
			"nested_dimension":   nestedDimension,
			"where":              getTimeseriesWhereSQL(config, anomaly, appID),
		},
	)
}

func prepareDistinctCategorySQL(
	config *cfg.ConfigType, anomaly *AnomalyType, categoryDimension string, appID string,
) string {
	return chpool.FormatSQLTemplate(
		anomaly.Templates.DistinctCategoryValues,
		map[string]interface{}{
			"db":                 config.Clickhouse.Database,
			"table":              getTableNameSQL(config, anomaly.Table),
			"category_dimension": categoryDimension,
			"where":              getTimeseriesWhereSQL(config, anomaly, appID),
		},
	)
}
func prepareNestedSQL(
	config *cfg.ConfigType,
	anomaly *AnomalyType, nestedDimension string, categoryDimension string,
	dateDimension *DateDimenstionType, appID string,
) string {
	return chpool.FormatSQLTemplate(anomaly.Templates.NestedDimensions, map[string]interface{}{
		"db":    config.Clickhouse.Database,
		"table": getTableNameSQL(config, anomaly.Table),
		//"metrics": "{metricName}",
		"nested_dimensions":         nestedDimension,
		"category_dimensions":       categoryDimension,
		"date_dimensions":           getDateDimensionsSQL(dateDimension),
		"where":                     getTimeseriesWhereSQL(config, anomaly, appID),
		"nested_dimensions_groupby": nestedDimension,
		"date_dimensions_groupby":   getDateDimensionsGroupBySQL(dateDimension),
	})
}
func prepareTimeSeriesSQL(
	config *cfg.ConfigType,
	template string, anomaly *AnomalyType, categoryDimension string,
	dateDimension *DateDimenstionType, appID string,
) string {
	return chpool.FormatSQLTemplate(template, map[string]interface{}{
		"db":                  config.Clickhouse.Database,
		"table":               getTableNameSQL(config, anomaly.Table),
		"metrics":             getMetricsSQL(anomaly),
		"category_dimensions": categoryDimension,
		"date_dimensions":     getDateDimensionsSQL(dateDimension),
		"where":               getTimeseriesWhereSQL(config, anomaly, appID),
		"category_dimensions_groupby": categoryDimension,
		"date_dimensions_groupby":     getDateDimensionsGroupBySQL(dateDimension),
	})
}

func getDateDimensionsGroupBySQL(dateDimension *DateDimenstionType) (sql string) {
	sql = dateDimension.Name
	if dateDimension.ExtractedFields != nil && len(dateDimension.ExtractedFields) > 0 {
		sql += ", "
		extLen := len(dateDimension.ExtractedFields)
		for extName := range dateDimension.ExtractedFields {
			sql += extName
			extLen--
			if extLen > 0 {
				sql += ", "
			}
		}
	}
	return sql
}

func getTimeseriesWhereSQL(config *cfg.ConfigType, anomaly *AnomalyType, defaultWhereValue string) (sql string) {
	sql = chpool.FormatSQLTemplate(anomaly.Where.Template, map[string]interface{}{
		"default_where_field": anomaly.Where.DefaultField,
		"default_where_value": defaultWhereValue,
		"date_where":          getDateDimensionWhereSQL(config.DateSince, config.DateUntil, anomaly),
	})
	return sql
}

func getTableNameSQL(config *cfg.ConfigType, tableName string) string {
	if config.Clickhouse.UseDistributed {
		return config.Clickhouse.TablePrefix + tableName + "_distributed"
	}
	return config.Clickhouse.TablePrefix + tableName + "_local"
}
func getDateDimensionsSQL(dateDimension *DateDimenstionType) (sql string) {
	sql = chpool.FormatSQLTemplate(
		"{value} AS {name}",
		map[string]interface{}{"name": dateDimension.Name, "value": dateDimension.Value},
	)
	if dateDimension.ExtractedFields != nil && len(dateDimension.ExtractedFields) > 0 {
		sql += ", "
		extLen := len(dateDimension.ExtractedFields)
		for extName, extValue := range dateDimension.ExtractedFields {
			sql += chpool.FormatSQLTemplate(
				"{value} AS {name}",
				map[string]interface{}{"name": extName, "value": extValue},
			)
			extLen--
			if extLen > 0 {
				sql += ", "
			}
		}
	}
	return sql
}

func getMetricsSQL(anomaly *AnomalyType) (sql string) {
	metricsCount := len(anomaly.Metrics)
	i := 1
	sql = ""
	for alias, value := range anomaly.Metrics {
		sql += fmt.Sprintf("%s AS %s", value, alias)
		if i < metricsCount {
			sql += ","
		}
		i++
	}
	return sql
}

func getAppIds(config *cfg.ConfigType, anomaly *AnomalyType) (appIds []string) {
	if strings.Contains(anomaly.Table, "appmetrika") && len(config.AppMetrika.ApplicationIds) > 0 {
		return config.AppMetrika.ApplicationIds
	} else if !strings.Contains(anomaly.Table, "appmetrika") && len(config.Metrika.CounterIds) > 0 {
		return config.Metrika.CounterIds
	}

	if !config.AutoLoadIds {
		return []string{}
	}

	db := chpool.GetRandomClickhouseConnection()
	q := chpool.FormatSQLTemplate(anomaly.Templates.DistinctCategoryValues, map[string]interface{}{
		"category_dimension": anomaly.Where.DefaultField,
		"db":                 config.Clickhouse.Database,
		"table":              getTableNameSQL(config, anomaly.Table),
		"where":              getDateDimensionWhereSQL(config.DateSince, config.DateUntil, anomaly),
	})
	rows, err := db.Query(q)
	if err != nil {
		log.Fatal().Err(err).Msg(q)
	}

	for rows.Next() {
		var appID uint32
		if err := rows.Scan(&appID); err != nil {
			log.Fatal().Err(err).Msg("getAppIds rows.Scan")
		}
		appIds = append(appIds, strconv.FormatUint(uint64(appID), 10))
	}
	return appIds
}

//@TODO 1h/DEV need more then 1 date dimension?
func getDateDimensionWhereSQL(dateSince, dateUntil time.Time, anomaly *AnomalyType) (sql string) {
	where := make([]string, len(anomaly.DateDimensions))
	for i, d := range anomaly.DateDimensions {
		where[i] = chpool.FormatSQLTemplate(d.WhereTemplate, map[string]interface{}{
			"value":     d.Value,
			"dateSince": dateSince.Format("2006-01-02 15:04:05"),
			"dateUntil": dateUntil.Format("2006-01-02 15:04:05"),
		})
	}
	return strings.Join(where, " AND ")
}
