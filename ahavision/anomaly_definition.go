package ahavision

import (
	"bitbucket.org/ahavision/anomalyzer/cfg"
	basecfg "bitbucket.org/clickhouse_pro/components/cfg"
	"github.com/lytics/anomalyzer"
	"github.com/rs/zerolog/log"
	"path/filepath"
)

//AnomaliesDefinitionsType describe all anomalies definitions
type AnomaliesDefinitionsType map[string]*AnomalyType

//AnomalyType describe anomaly definition
type AnomalyType struct {
	Table     string        `yaml:"table"`
	Templates TemplatesType `yaml:"templates"`
	Where     WhereType

	// @TODO 8h/DEV think about complex metric types
	// metric_name -> metric_sql_value
	Metrics            map[string]string
	DateDimensions     []*DateDimenstionType            `yaml:"date_dimensions"`
	CategoryDimensions map[string]CategoryDimensionType `yaml:"category_dimensions"`
	AnomalyzerConf     *anomalyzer.AnomalyzerConf       `yaml:"anomalyzer_conf"`
}

//WhereType desribe whole WHERE filter for Table
type WhereType struct {
	DefaultField string `yaml:"default_field"`
	Template     string
}

//TemplatesType every field contains SQL template for some queries need for evalute timeseries data
type TemplatesType struct {
	GlobalMetricsValues    string `yaml:"global_metrics_values"`
	DistinctCategoryValues string `yaml:"disticnt_category_values"`
	TimeSeries             string `yaml:"time_series"`
	NestedDimensions       string `yaml:"nested_dimensions"`
	NestedDistinctValues   string `yaml:"nested_distinct_values"`
}

//CategoryDimensionType every category dimension described in format mainDimenstion -> nestedDimensions comma separated list
type CategoryDimensionType map[string]string

//DateDimenstionType every date dimension describe how filter Table by date period
type DateDimenstionType struct {
	Name            string
	Value           string
	WhereTemplate   string            `yaml:"where_template"`
	ExtractedFields map[string]string `yaml:"extracted_fields"`
}

//AnomaliesDefinitions main struct for anomalies definitions
var AnomaliesDefinitions = AnomaliesDefinitionsType{
	"metrika_visits_simple": &AnomalyType{
		Table: "metrika_visits",
		Templates: TemplatesType{
			GlobalMetricsValues:    "SELECT {metrics}, {date_dimensions} FROM {db}.{table} WHERE {where} GROUP BY {date_dimensions_groupby}",
			DistinctCategoryValues: "SELECT DISTINCT {category_dimension} FROM {db}.{table} WHERE {where}",
			TimeSeries:             "SELECT {metrics}, {category_dimensions}, {date_dimensions} FROM {db}.{table} WHERE {where} GROUP BY {category_dimensions_groupby}, {date_dimensions_groupby}",
			NestedDimensions:       "SELECT {metrics}, {nested_dimensions}, {date_dimensions} FROM {db}.{table} WHERE {category_dimensions}=? AND {where} GROUP BY {nested_dimensions_groupby}, {date_dimensions_groupby}",
			NestedDistinctValues:   "SELECT DISTINCT {nested_dimension} FROM {db}.{table} WHERE {category_dimension}=? AND {where}",
		},
		Where: WhereType{
			DefaultField: "CounterID",
			Template:     "{default_where_field}={default_where_value} AND {date_where}",
		},

		// @TODO 2h/DEV think about complex metric types
		// metric_name -> metric_value
		Metrics: map[string]string{
			"uniq_users":   "toFloat64(uniq(ClientID))",
			"bounce_users": "toFloat64(uniq( if(Bounce,ClientID,0) )-1)",
			//"uniq_visits": "toFloat64(uniq(VisitID))",
			//"bounce_visits": "toFloat64(uniq(if(Bounce,ClientID,0))-1)",
		},
		//dimension_item -> dimension_info
		DateDimensions: []*DateDimenstionType{
			{
				Name:          "Date",
				Value:         "Date",
				WhereTemplate: "{value} BETWEEN toDate('{dateSince}') AND toDate('{dateUntil}')",
				//ExtractedFields: map[string]string{
				//	"week_num":    "toRelativeWeekNum(Date)-toRelativeWeekNum(toStartOfYear(Date))",
				//	"is_weekend":  "( toDayOfWeek(Date) BETWEEN 6 AND 7 ? 1 : 0)",
				//	"season_name": "multiIf( toMonth(Date) IN (12,1,2), 'Winter', toMonth(Date) IN (3,4,5), 'Spring', toMonth(Date) IN (6,7,8), 'Summer',toMonth(Date) IN (9,10,11), 'Autum', 'Unknown')",
				//},
			},
		},
		// group_name -> field_name -> nested_fields, comma_separated
		CategoryDimensions: map[string]CategoryDimensionType{
			"lastSourceDimentionsGroup": {
				"LastTrafficSource": "",
				"LastAdvEngine":     "",
				"LastReferalSource": "",
				"LastSearchEngine":  "LastSearchEngineRoot",
				"LastSocialNetwork": "",
			},
			"lastTaggedTrafficDimentionsGroup": {
				"From":             "",
				"UTMCampaign":      "UTMSource,UTMMedium",
				"UTMContent":       "UTMSource,UTMMedium",
				"UTMMedium":        "UTMSource,UTMContent",
				"UTMSource":        "UTMCampaign,UTMMedium,UTMContent",
				"UTMTerm":          "UTMSource,UTMMedium,UTMContent",
				"OpenstatAd":       "OpenstatSource,OpenstatCampaign,OpenstatService",
				"OpenstatCampaign": "OpenstatSource,OpenstatService",
				"OpenstatService":  "OpenstatSource,OpenstatCampaign",
				"OpenstatSource":   "OpenstatService,OpenstatCampaign",
			},
			"geoTrafficDimensionsGroup": {
				"RegionCountry":   "RegionCity",
				"RegionCity":      "",
				"BrowserCountry":  "RegionCity",
				"BrowserLanguage": "RegionCity,BrowserCountry,BrowserEngine",
			},
			"deviceParamsDimensionsGroup": {
				"DeviceCategory": "",
				//"MobilePhone":         "MobilePhoneModel",
				//"MobilePhoneModel":    "",
				"OperatingSystemRoot": "",
				"OperatingSystem":     "",
				"Browser":             "OperatingSystem",
			},
		},
		AnomalyzerConf: &anomalyzer.AnomalyzerConf{
			Sensitivity: 0.1,
			//LowerBound:  0,
			//UpperBound: 0,
			ActiveSize: 7,
			NSeasons:   3,
			Methods:    []string{"diff", "magnitude", "ks", "cdf"}, // "fence", "highrank", "lowrank"
		},
	},

	"appmetrika_installations_simple": &AnomalyType{
		Table: "appmetrika_installations",
		Templates: TemplatesType{
			GlobalMetricsValues:    "SELECT {metrics}, {date_dimensions} FROM {db}.{table} WHERE {where} GROUP BY {date_dimensions_groupby}",
			DistinctCategoryValues: "SELECT DISTINCT {category_dimension} FROM {db}.{table} WHERE {where}",
			TimeSeries:             "SELECT {metrics}, {category_dimensions}, {date_dimensions} FROM {db}.{table} WHERE {where} GROUP BY {category_dimensions_groupby}, {date_dimensions_groupby}",
			NestedDimensions:       "SELECT {metrics}, {nested_dimensions}, {date_dimensions} FROM {db}.{table} WHERE {category_dimensions}=? AND {where} GROUP BY {nested_dimensions_groupby}, {date_dimensions_groupby}",
			NestedDistinctValues:   "SELECT DISTINCT {nested_dimension} FROM {db}.{table} WHERE {category_dimension}=? AND {where}",
		},
		Where: WhereType{
			DefaultField: "app_id",
			Template:     "{default_where_field}={default_where_value} AND {date_where}",
		},

		// @TODO 2h/DEV think about complex metric types
		// metric_name -> metric_value
		Metrics: map[string]string{
			"installs":   "toFloat64(uniq(appmetrica_device_id))",
			"reinstalls": "toFloat64(uniq( if(is_reinstallation='true',appmetrica_device_id,'0') )-1)",
		},
		//dimension_item -> dimension_info
		DateDimensions: []*DateDimenstionType{
			{
				Name:          "InstallDate",
				Value:         "install_date",
				WhereTemplate: "{value} BETWEEN toDate('{dateSince}') AND toDate('{dateUntil}')",
			},
		},
		// group_name -> field_name -> nested_fields, comma_separated
		CategoryDimensions: map[string]CategoryDimensionType{
			"sourceDimentionsGroup": {
				"publisher_name": "",
				"tracker_name":   "",
				// @TODO talk with AppMetrika Team about extract fields from this field for organic installations
				"click_url_parameters": "",
			},
			"appDimentionsGroup": {
				"app_version_name": "app_package_name",
			},
			"geoDimentionsGroup": {
				"iso_country_code": "city",
				"operator_name":    "city,device_locale,device_manufacturer,device_model",
			},
			"deviceDimentionsGroup": {
				"device_manufacturer": "device_model,device_locale,device_type",
				"device_model":        "",
				"os_name":             "os_version",
				"os_version":          "os_name",
			},
			"connectDimensionGroup": {
				"connection_type": "operator_name",
			},
		},
		AnomalyzerConf: &anomalyzer.AnomalyzerConf{
			Sensitivity: 0.1,
			//LowerBound:  0,
			//UpperBound: 0,
			ActiveSize: 7,
			NSeasons:   3,
			Methods:    []string{"diff", "magnitude", "ks", "cdf"}, // "fence", "highrank", "lowrank"
		},
	},

	"appmetrika_crashes_simple": &AnomalyType{
		Table: "appmetrika_crashes",
		Templates: TemplatesType{
			GlobalMetricsValues:    "SELECT {metrics}, {date_dimensions} FROM {db}.{table} WHERE {where} GROUP BY {date_dimensions_groupby}",
			DistinctCategoryValues: "SELECT DISTINCT {category_dimension} FROM {db}.{table} WHERE {where}",
			TimeSeries:             "SELECT {metrics}, {category_dimensions}, {date_dimensions} FROM {db}.{table} WHERE {where} GROUP BY {category_dimensions_groupby}, {date_dimensions_groupby}",
			NestedDimensions:       "SELECT {metrics}, {nested_dimensions}, {date_dimensions} FROM {db}.{table} WHERE {category_dimensions}=? AND {where} GROUP BY {nested_dimensions_groupby}, {date_dimensions_groupby}",
			NestedDistinctValues:   "SELECT DISTINCT {nested_dimension} FROM {db}.{table} WHERE {category_dimension}=? AND {where}",
		},
		Where: WhereType{
			DefaultField: "app_id",
			Template:     "{default_where_field}={default_where_value} AND {date_where}",
		},

		// metric_name -> metric_value
		Metrics: map[string]string{
			"crashed_devices": "toFloat64(uniq(appmetrica_device_id))",
		},
		//dimension_item -> dimension_info
		DateDimensions: []*DateDimenstionType{
			{
				Name:          "CrashDate",
				Value:         "crash_date",
				WhereTemplate: "{value} BETWEEN toDate('{dateSince}') AND toDate('{dateUntil}')",
			},
		},
		// group_name -> field_name -> nested_fields, comma_separated
		CategoryDimensions: map[string]CategoryDimensionType{
			"crashDimentionsGroup": {
				"crash_group_id": "crash",
			},
			"appDimentionsGroup": {
				"app_version_name": "app_package_name",
			},
			"geoDimentionsGroup": {
				"city":             "",
				"country_iso_code": "",
				"operator_name":    "device_locale",
			},
			"deviceDimentionsGroup": {
				"device_manufacturer": "device_model,device_locale,device_type",
				"device_model":        "",
				"os_name":             "os_version",
				"os_version":          "os_name",
			},
			"connectDimensionGroup": {
				"connection_type": "operator_name",
			},
		},
		AnomalyzerConf: &anomalyzer.AnomalyzerConf{
			Sensitivity: 0.1,
			//LowerBound:  0,
			//UpperBound: 0,
			ActiveSize: 7,
			NSeasons:   3,
			Methods:    []string{"diff", "magnitude", "ks", "cdf"}, // "fence", "highrank", "lowrank"
		},
	},
}

//AnomalyDefinitionsConfigType type for describe yaml structure of anomalies definitions
type AnomalyDefinitionsConfigType = struct {
	AnomaliesDefinitions AnomaliesDefinitionsType `yaml:"anomalies_definitions"`
}

//AnomalyDefinitionsConfig structure for load anomalies definitions from yaml file
var AnomalyDefinitionsConfig = AnomalyDefinitionsConfigType{
	AnomaliesDefinitions: AnomaliesDefinitions,
}

//LoadAnomaliesDefinitions fill AnomalyDefinitionsConfig from filepath.Glob pattern
func LoadAnomaliesDefinitions(config *cfg.ConfigType) {
	config.LoadConfigFile(&AnomalyDefinitionsConfig, basecfg.ConfigFile, basecfg.Environment)
	if config.Anomalyzer.AnomalyDefinitionFiles != "" {
		if definitionFiles, err := filepath.Glob(config.Anomalyzer.AnomalyDefinitionFiles); err == nil {
			for _, f := range definitionFiles {
				config.LoadConfigFile(&AnomalyDefinitionsConfig, &f, basecfg.Environment)
			}
		} else {
			log.Error().Err(err).
				Str("anomalyzer.anomaly_definitions_files", config.Anomalyzer.AnomalyDefinitionFiles).
				Msg("wrong anomalyzer.anomaly_definitions_files pattern")
		}
	}
}

//SaveAnomaliesDefinitions fill AnomalyDefinitionsConfig from filepath.Glob pattern
func SaveAnomaliesDefinitions(config *cfg.ConfigType) {
	baseDir := filepath.Dir(*basecfg.ConfigFile)
	if config.Anomalyzer.AnomalyDefinitionFiles != "" {
		baseDir = filepath.Dir(config.Anomalyzer.AnomalyDefinitionFiles)
	}
	for definitionName, definitionContent := range AnomalyDefinitionsConfig.AnomaliesDefinitions {
		var OnceDefinitionConfig = AnomalyDefinitionsConfigType{
			AnomaliesDefinitionsType{
				definitionName: definitionContent,
			},
		}
		anomalyDefinitionsYml := filepath.Join(baseDir, definitionName+".yml")
		config.SaveConfigFile(&OnceDefinitionConfig, &anomalyDefinitionsYml, basecfg.Environment)
	}
}
