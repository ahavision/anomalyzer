package ahavision

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"text/template"
	"time"

	"bitbucket.org/ahavision/anomalyzer/cfg"

	"github.com/rs/zerolog/log"

	"github.com/wcharczuk/go-chart"
	"github.com/wcharczuk/go-chart/drawing"
)

//FileReporter save founded anomalies into cfg.Reporter.ReportDir
type FileReporter struct {
	ReporterInterface
}

type templateDataType struct {
	Header    string
	MainPNG   string
	MainTitle string
	Nested    map[string]string
}

//ReportAnomaly save founded anomalies into cfg.Reporter.ReportDir
func (r *FileReporter) ReportAnomaly(
	config *cfg.ConfigType,
	anomaly *AnomalyType,
	appID, anomalyName, categoryDimension, categoryValue, metricName, metricValue string,
	dateNames []time.Time, tsData []float64, nestedTS map[string]map[string][]float64,
	probability, impactPercent float64,
) {
	var err error
	defer dataSyncronizer.reportData.Done()
	defer func() {
		if err != nil {
			log.Fatal().
				Err(err).
				Str("appID", appID).
				Str("anomalyName", anomalyName).
				Float64("probability", probability).
				Float64("impactPercent", impactPercent).
				Str(categoryDimension, categoryValue).
				Str(metricName, metricValue).
				Floats64("points", tsData).
				Str("nestedTS", fmt.Sprintf("%v", nestedTS)).
				Msgf("ReportAnomaly Error")
		} else {
			log.Debug().
				Str("appID", appID).
				Str("anomalyName", anomalyName).
				Float64("probability", probability).
				Float64("impactPercent", impactPercent).
				Str(categoryDimension, categoryValue).
				Str(metricName, metricValue).
				Floats64("points", tsData).
				Str("nestedTS", fmt.Sprintf("%v", nestedTS)).
				Msgf("ReportAnomaly End")
		}
	}()
	log.Debug().
		Str("appID", appID).
		Str("anomalyName", anomalyName).
		Float64("probability", probability).
		Float64("impactPercent", impactPercent).
		Str(categoryDimension, categoryValue).
		Str(metricName, metricValue).
		Floats64("points", tsData).
		Str("nestedTS", fmt.Sprintf("%v", nestedTS)).
		Msgf("ReportAnomaly Begin")

	aBegin := len(tsData) - anomaly.AnomalyzerConf.ActiveSize
	reportDir := filepath.Join(config.Reporter.ReportDir, appID, anomalyName)

	reportName := cleanFileName(fmt.Sprintf(
		"main.%s.%s.%s.from.%s.to.%s",
		metricName,
		categoryDimension,
		categoryValue,
		dateNames[0].Format("2006-01-02"),
		dateNames[len(dateNames)-1].Format("2006-01-02"),
	))

	mainTitle := fmt.Sprintf(
		"%v=%v, %v anomaly=%0.2f%% impact=%0.2f%%",
		categoryDimension, categoryValue, metricName, probability*100, impactPercent*100,
	)
	mainChart := chart.Chart{
		Width:  800,
		Height: 300,
		XAxis: chart.XAxis{
			Name:      mainTitle,
			NameStyle: chart.StyleShow(),
			Style:     chart.StyleShow(),
		},
		YAxis: chart.YAxis{
			Name:      metricName,
			NameStyle: chart.StyleShow(),
			Style:     chart.StyleShow(),
		},
		Series: []chart.Series{
			chart.TimeSeries{
				Name: metricName,
				Style: chart.Style{
					Show:        true,
					StrokeColor: drawing.ColorBlue,
					FillColor:   drawing.ColorBlue,
				},
				XValues: dateNames[:aBegin+1],
				YValues: tsData[:aBegin+1],
			},
			chart.TimeSeries{
				Name: "anomaly",
				Style: chart.Style{
					Show:        true,
					StrokeColor: drawing.ColorRed,
					FillColor:   drawing.ColorRed,
				},
				XValues: dateNames[aBegin:],
				YValues: tsData[aBegin:],
			},
		},
	}

	pngName := fmt.Sprintf("%s.png", reportName)
	err = saveChartToPNG(mainChart, reportDir, pngName)
	if err != nil {
		return
	}
	var tmpl *template.Template
	tmpl, err = template.ParseFiles(filepath.Join(config.Reporter.TemplateDir, "report.html"))
	if err != nil {
		return
	}

	htmlName := fmt.Sprintf("%s.html", reportName)
	tmplData := templateDataType{
		Header:    "Anomaly Detected",
		MainPNG:   pngName,
		MainTitle: mainTitle,
		Nested:    make(map[string]string, len(nestedTS)),
	}

	err = reportNestedAnomalies(metricName, nestedTS, dateNames, reportDir, &tmplData)
	if err != nil {
		return
	}
	var f *os.File
	if f, err = os.OpenFile(filepath.Join(reportDir, htmlName), os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600); err == nil {
		if err = tmpl.Execute(f, tmplData); err == nil {
			err = f.Close()
		}
	}
}

func reportNestedAnomalies(
	metricName string,
	nestedTS map[string]map[string][]float64, dateNames []time.Time,
	reportDir string, tmplData *templateDataType,
) (err error) {
	for nestedDimension, nestedValues := range nestedTS {
		if len(nestedValues) == 0 {
			continue
		}
		nestedSeries := make([]chart.Series, len(nestedValues))
		seriesIdx := 0
		for nValue, nestedTsData := range nestedValues {
			nestedSeries[seriesIdx] = chart.TimeSeries{
				Name: nValue,
				Style: chart.Style{
					Show:     true,
					DotColor: chart.GetDefaultColor(seriesIdx),
				},
				XValues: dateNames,
				YValues: nestedTsData,
			}
			seriesIdx++
		}
		nestedHeight := 40 * len(nestedValues)
		if nestedHeight < 400 {
			nestedHeight = 400
		}
		nestedChart := chart.Chart{
			Width:  800,
			Height: nestedHeight,
			XAxis: chart.XAxis{
				Name:      nestedDimension,
				NameStyle: chart.StyleShow(),
				Style:     chart.StyleShow(),
			},
			YAxis: chart.YAxis{
				Name:      metricName,
				NameStyle: chart.StyleShow(),
				Style:     chart.StyleShow(),
			},
			Series: nestedSeries,
		}
		nestedChart.Elements = []chart.Renderable{
			chart.LegendLeft(&nestedChart),
		}

		nestedPngName := cleanFileName(fmt.Sprintf(
			"nested.%s.%s.from.%s.to.%s.png",
			metricName,
			nestedDimension,
			dateNames[0].Format("2006-01-02"),
			dateNames[len(dateNames)-1].Format("2006-01-02"),
		))
		nestedTitle := fmt.Sprintf(
			"%s %s from %s to %s",
			metricName,
			nestedDimension,
			dateNames[0].Format("2006-01-02"),
			dateNames[len(dateNames)-1].Format("2006-01-02"),
		)
		err = saveChartToPNG(nestedChart, reportDir, nestedPngName)
		if err != nil {
			return err
		}
		tmplData.Nested[nestedTitle] = nestedPngName
	}
	return nil
}

func cleanFileName(fileName string) string {
	fileName = strings.Replace(fileName, ":", "_", -1)
	fileName = strings.Replace(fileName, "/", "_", -1)
	return fileName
}

func saveChartToPNG(dataChart chart.Chart, reportDir string, pngName string) error {
	var err error
	var f *os.File
	if err = os.MkdirAll(reportDir, 0700); err == nil {
		if f, err = os.OpenFile(filepath.Join(reportDir, pngName), os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600); err == nil {
			if err = dataChart.Render(chart.PNG, f); err == nil {
				log.Debug().Str("pngFile", filepath.Join(reportDir, pngName)).Msg("saveChartToPNG")
				err = f.Close()
			}
		}
	}
	return err
}
