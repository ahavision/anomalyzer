# Anomalyzer
Find anomalies in any clickhouse databases

# Howto install development environment

```bash
git clone
cd anomalyzer
vagrant up --provision
```
