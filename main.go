package main

import (
	"bitbucket.org/ahavision/anomalyzer/ahavision"
	"bitbucket.org/ahavision/anomalyzer/cfg"
	basecfg "bitbucket.org/clickhouse_pro/components/cfg"
	"bitbucket.org/clickhouse_pro/components/chpool"
	"bitbucket.org/clickhouse_pro/components/zkpool"
)

func main() {
	config := cfg.NewConfig()
	basecfg.InitConfig(config)
	zkpool.InitZookeeper(&config.ConfigType)
	chpool.InitClickhousePool(&config.ConfigType)
	chpool.DetectAllowDistributed(&config.ConfigType)
	chpool.DetectAllowReplicated(&config.ConfigType)
	if config.Anomalyzer.DetectAnomalies {
		ahavision.LoadAnomaliesDefinitions(config)
		ahavision.SaveAnomaliesDefinitions(config)
		ahavision.InitDataSyncronyzer(config)
		ahavision.DetectAnomalies(config)
	}
}
