FROM golang:alpine AS builder
MAINTAINER Eugene Klimov <bloodjazman@gmail.com>

WORKDIR /anomalyzer/
VOLUME /var/www/anomalies

ENV GOPATH /anomalyzer/

COPY ./.gometalinter.json /anomalyzer/
COPY ./ /anomalyzer/src/bitbucket.org/ahavision/anomalyzer/
COPY ./id_rsa /root/.ssh/id_rsa
RUN chmod 0600 /root/.ssh/id_rsa

RUN apk add --no-cache --update git openssh-client
RUN touch /root/.ssh/known_hosts
RUN ssh-keygen -R github.com
RUN ssh-keygen -R bitbucket.org
RUN ssh-keyscan -H github.com >> /root/.ssh/known_hosts
RUN ssh-keyscan -H bitbucket.org >> /root/.ssh/known_hosts
RUN git config --global url."git@github.com:".insteadOf "https://github.com/"
RUN git config --global url."git@bitbucket.org:".insteadOf "https://bitbucket.org/"


#RUN mkdir -m 0755 -p /var/lib/GeoIP/
#ADD http://geolite.maxmind.com/download/geoip/database/GeoLite2-City.mmdb.gz /var/lib/GeoIP/GeoLite2-City.mmdb.gz
#RUN gzip -d /var/lib/GeoIP/GeoLite2-City.mmdb.gz
#RUN echo "wget -O - http://geolite.maxmind.com/download/geoip/database/GeoLite2-City.mmdb.gz | gzip -d > /var/lib/GeoIP/GeoLite2-City.mmdb" > /etc/periodic/weekly/geoiplite2


RUN mkdir -p /anomalyzer/bin
RUN cd /anomalyzer/src/bitbucket.org/ahavision/anomalyzer && go get -v ./...

RUN go get -u github.com/alecthomas/gometalinter
RUN /anomalyzer/bin/gometalinter --install
RUN /anomalyzer/bin/gometalinter --config=/anomalyzer/.gometalinter.json --deadline=600s --exclude=/go/src/ /anomalyzer/src/bitbucket.org/ahavision/anomalyzer/ahavision/...

RUN go test -v bitbucket.org/ahavision/anomalyzer/ahavision \
    && go build -o /anomalyzer/bin/anomalyzer /anomalyzer/src/bitbucket.org/ahavision/anomalyzer/main.go \
    && rm -rf /anomalyzer/src


FROM alpine:latest
ENV DOCKERIZE_VERSION v0.4.0
RUN apk add --no-cache --update ca-certificates openssl \
		&& apk add --no-cache --update gzip sed \
    && update-ca-certificates \
    && wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz

COPY --from=builder /anomalyzer/bin/anomalyzer /anomalyzer/bin/
COPY --from=builder /usr/local/go/lib/time/zoneinfo.zip /usr/local/go/lib/time/zoneinfo.zip
COPY ./templates /templates
ENTRYPOINT ["/bin/sh","-c"]