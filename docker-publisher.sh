#!/bin/bash
set -xeuo pipefail
echo 1 > /proc/sys/vm/drop_caches
docker-compose down
docker system prune -f
docker login -u clickhousepro cloud.canister.io:5000
docker-compose pull metrika2clickhouse
docker login -u ahavision cloud.canister.io:5000
docker-compose build anomalyzer
docker-compose run --rm metrika2clickhouse
docker-compose run --rm anomalyzer
docker-compose push anomalyzer
