module bitbucket.org/ahavision/anomalyzer

go 1.13

require (
	bitbucket.org/clickhouse_pro/components v0.0.0-20191012175805-8cefd664d0f5
	github.com/Slach/Recast-Golang-SDK v0.0.0-20180213105712-4550656f6cfc
	github.com/blend/go-sdk v2.0.0+incompatible // indirect
	github.com/bmizerany/assert v0.0.0-20160611221934-b7ed37b82869 // indirect
	github.com/drewlanenga/govector v0.0.0-20160727150047-f69e9f02317e // indirect
	github.com/elazarl/goproxy v0.0.0-20191011121108-aa519ddbe484 // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/jarcoal/httpmock v1.0.4 // indirect
	github.com/jinzhu/configor v1.1.1 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/kshvakov/clickhouse v1.3.11 // indirect
	github.com/levigross/grequests v0.0.0-20190908174114-253788527a1a // indirect
	github.com/lytics/anomalyzer v0.0.0-20151102000650-13cee1061701
	github.com/parnurzeal/gorequest v0.2.16 // indirect
	github.com/rs/zerolog v1.15.0
	github.com/samuel/go-zookeeper v0.0.0-20190923202752-2cc03de413da // indirect
	github.com/smartystreets/goconvey v0.0.0-20190731233626-505e41936337 // indirect
	github.com/wcharczuk/go-chart v2.0.1+incompatible
	golang.org/x/image v0.0.0-20191009234506-e7c1f5e7dbb8 // indirect
	golang.org/x/oauth2 v0.0.0-20190604053449-0f29369cfe45 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/pool.v3 v3.1.1
	gopkg.in/yaml.v2 v2.2.4 // indirect
	moul.io/http2curl v1.0.0 // indirect
)
