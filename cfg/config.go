package cfg

import (
	"flag"

	"bitbucket.org/clickhouse_pro/components/cfg"
	"os"
	"path/filepath"
	"runtime"
	"strings"
)

//ConfigType main anomalyzer config structure
type ConfigType struct {
	cfg.ConfigType `yaml:",inline"`
	Anomalyzer     struct {
		AnomalyDefinitionFiles string  `yaml:"anomaly_definition_files"`
		ParallelJob            uint    `yaml:"parallel_job"`
		ImpactPercent          float64 `yaml:"impact_percent"`
		Probability            float64 `yaml:"probability"`
		NestedProbability      float64 `yaml:"nested_probability"`
		DetectAnomalies        bool    `yaml:"detect_anomalies"`
	}
	Reporter struct {
		Types                 []string `yaml:"types"`
		ReportDir             string   `yaml:"report_dir"`
		BaseURL               string   `yaml:"base_url"`
		TemplateDir           string   `yaml:"template_dir"`
		RecastRequestToken    string   `yaml:"recast_request_token"`
		RecastDeveloperToken  string   `yaml:"recast_developer_token"`
		RecastConversationIDs []string `yaml:"recast_conversation_ids"`
	} `yaml:"reporter"`
}

var recastConversationIDs *string
var reporterTypes *string

//NewConfig create main config structure
func NewConfig() *ConfigType {
	return &ConfigType{}
}

//DefineCLIFlags composition of components/cfg DefineCLIFlags and "nested" DefineCLIFlags
func (c *ConfigType) DefineCLIFlags() {
	c.ConfigType.DefineCLIFlags()

	flag.BoolVar(&c.Anomalyzer.DetectAnomalies, "anomalyzer.detect", true, "detect anomalies")
	flag.UintVar(&c.Anomalyzer.ParallelJob, "anomalyzer.parallel_job", uint(runtime.NumCPU()), "how many goroutines run parallel when anomaly detection")
	flag.Float64Var(&c.Anomalyzer.Probability, "anomalyzer.probability", 0.90, "anomaly probability, report anomalies which probability above X")
	flag.Float64Var(&c.Anomalyzer.NestedProbability, "anomalyzer.nested_probability", 0.90, "anomaly probability for nested dimensions, report nested anomalies which probability above X")
	flag.Float64Var(&c.Anomalyzer.ImpactPercent, "anomalyzer.impact_percent", 0.03, "when anomaly detect, show only anomalies which impact to total values more than X")
	flag.StringVar(&c.Anomalyzer.AnomalyDefinitionFiles, "anomalyzer.anomaly_definition_files", "", "load custom anomaly definitions from YAML format (use https://golang.org/pkg/path/filepath/#Glob syntax)")

	//nolint: gas
	cwd, _ := os.Getwd()
	// @TODO 16h/DEV implements trello and jira reporter
	reporterTypes = flag.String("reporter.types", "file,recast", "comma separated list of reporter types. Example: file,recast,trello,jira")
	recastConversationIDs = flag.String("reporter.recast_conversation_ids", "", "Recast.ai conversation IDs for send messages about anomalies")

	flag.StringVar(&c.Reporter.BaseURL, "reporter.base_url", "http://anomalyzer.ahavision.com/anomalies", "report directory for save html + png format")
	flag.StringVar(&c.Reporter.ReportDir, "reporter.report_dir", "/var/www/anomalies", "report directory for save html + png format")
	flag.StringVar(&c.Reporter.TemplateDir, "reporter.template_dir", filepath.Join(cwd, "/templates/"), "templates sub directory for report generation")
	flag.StringVar(&c.Reporter.RecastRequestToken, "reporter.recast_request_token", "", "Recast.ai request token see https://recast.ai/slach/ahavision/settings")
	flag.StringVar(&c.Reporter.RecastDeveloperToken, "reporter.recast_developer_token", "", "Recast.ai request token see https://recast.ai/slach/ahavision/settings")

}

//MergeConfigFileWithCLIFlags composition merge parsed CLI flags and cfg.ConfigType structure
func (c *ConfigType) MergeConfigFileWithCLIFlags() {
	c.ConfigType.MergeConfigFileWithCLIFlags()
	if (c.Reporter.Types == nil || len(c.Reporter.Types) == 0) && *reporterTypes != "" {
		c.Reporter.Types = strings.Split(*reporterTypes, ",")
	}
	if (c.Reporter.RecastConversationIDs == nil || len(c.Reporter.RecastConversationIDs) == 0) && *recastConversationIDs != "" {
		c.Reporter.RecastConversationIDs = strings.Split(*recastConversationIDs, ",")
	}
}
